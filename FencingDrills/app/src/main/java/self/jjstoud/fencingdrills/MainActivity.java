package self.jjstoud.fencingdrills;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button buttonD1 = (Button) findViewById(R.id.buttonD1);
        final Button buttonD2 = (Button) findViewById(R.id.buttonD2);


        buttonD1.setText("Basic Footwork");
        buttonD2.setText("Advanced Footwork");

        View.OnClickListener onClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent;

                switch (v.getId())
                {
                    case R.id.buttonD1:
                        intent = new Intent(getBaseContext(), BasicFootworkActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.buttonD2:
                        intent = new Intent(getBaseContext(), AdvancedFootworkActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        };

        buttonD1.setOnClickListener(onClickListener);
        buttonD2.setOnClickListener(onClickListener);
    }
}
