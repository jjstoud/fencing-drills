package self.jjstoud.fencingdrills;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class BasicFootworkActivity extends AppCompatActivity
{
    public int c = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_footwork);

        final TextView timeTV = (TextView) findViewById(R.id.textTime2);
        final TextView incrementTV = (TextView) findViewById(R.id.textIncrement2);
        
        final TextView textDrill = (TextView) findViewById(R.id.textDrill);

        final Button buttonStart = (Button) findViewById(R.id.startButton);
        buttonStart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final int timeMin = Integer.parseInt(timeTV.getText().toString());
                int incrementSec = Integer.parseInt(incrementTV.getText().toString());

                Timer timer = new Timer();

                timer.scheduleAtFixedRate(new TimerTask()
                {
                    long t0 = System.currentTimeMillis();
                    boolean lunged = false;

                    @Override
                    public void run()
                    {
                        if (System.currentTimeMillis() - t0 > timeMin * 60000)
                        {
                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    textDrill.setText("Drills Done");
                                }
                            });

                            cancel();
                        }

                        else
                        {
                            Random rand = new Random();
                            int drillNum;
                            final String drill;

                            if (lunged)
                            {
                                drillNum = rand.nextInt(4) + 1;

                                if (drillNum == 1)
                                {
                                    drill = "Recover Up";
                                    lunged = false;
                                }

                                else if (drillNum == 2)
                                {
                                    drill = "Recover Forward";
                                    lunged = false;
                                }

                                else if (drillNum == 3)
                                {
                                    drill = "Recover Back";
                                    lunged = false;
                                }

                                else
                                {
                                    drill = "Redouble";
                                }
                            }

                            else
                            {
                                drillNum = rand.nextInt(3) + 1;

                                if (drillNum == 1)
                                {
                                    drill = "Advance";
                                }

                                else if (drillNum == 2)
                                {
                                    drill = "Retreat";
                                }

                                else
                                {
                                    drill = "Lunge";
                                    lunged = true;
                                }
                            }

                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (c == 1)
                                    {
                                        textDrill.setTextColor(Color.BLUE);
                                        textDrill.setText(drill);
                                        setColor(2);
                                    }

                                    else
                                    {
                                        textDrill.setTextColor(Color.RED);
                                        textDrill.setText(drill);
                                        setColor(1);
                                    }
                                }
                            });
                        }
                    }
                }, 0, incrementSec * 1000);
            }
        });
    }

    private void setColor(int c)
    {
        this.c = c;
    }

    public int getColor()
    {
        return c;
    }
}
